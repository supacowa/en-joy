local class, get, set = unpack(require "class")


local WIDTH = 256
local HEIGHT = 144
local ZOOM = 3


local window = am.window {
    title = "en~joy (working title)",
    mode = "windowed",
    projection = math.ortho(0, WIDTH, 0, HEIGHT, -1, 1),
    width = WIDTH * ZOOM,
    height = HEIGHT * ZOOM,
    orientation = nil, -- "portrait", "landscape" or nil for both
    resizable = true,
    letterbox = true,
    borderless = false,
    clear_color = vec4(.5, .5, .5, 1),
    highdpi = false,
    msaa_samples = 0,
    depth_buffer = false,
    stencil_buffer = false,
    show_cursor = true,
    lock_pointer = false
}
