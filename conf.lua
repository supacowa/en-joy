-- TODO check if these variables must be global or are allowed to be local?

local title = "en~joy"
local shortname = "en~joy"
local display_name = "en~joy"
local icon = "icon.png"
local launch_image = "launch.png"
local orientation = "any"
local author = "jack0088"
local appid = "de.herrsch"
local dev_region = "de"
local supported_languages = "de,en,ru"
local version = "1.0.0"
